## [1.0.12](https://gitlab.com/appframework/egore-personal/pagespeed/compare/1.0.11...1.0.12) (2025-02-03)


### Bug Fixes

* **deps:** update quarkus.platform.version to v3.18.1 ([4af3db7](https://gitlab.com/appframework/egore-personal/pagespeed/commit/4af3db789ebc6947d1f293b7ff6d7351337049d4))

## [1.0.11](https://gitlab.com/appframework/egore-personal/pagespeed/compare/1.0.10...1.0.11) (2025-01-28)


### Bug Fixes

* **deps:** update dependency org.jsoup:jsoup to v1.18.3 ([e10e637](https://gitlab.com/appframework/egore-personal/pagespeed/commit/e10e6375e65cd02aaa27b5d36238a9e0bcc18ece))
* **deps:** update quarkus.platform.version to v3.17.5 ([3e4451a](https://gitlab.com/appframework/egore-personal/pagespeed/commit/3e4451af151fad47e446b9c2d258b0744466d3d2))
* **deps:** update quarkus.platform.version to v3.17.6 ([c8b9d30](https://gitlab.com/appframework/egore-personal/pagespeed/commit/c8b9d307afdc6c5b8880cf44aa2712e491d16778))
* **deps:** update quarkus.platform.version to v3.17.8 ([b3e9e82](https://gitlab.com/appframework/egore-personal/pagespeed/commit/b3e9e828de0e5a7a6ec34ad0efe397330c0533c4))

## [1.0.10](https://gitlab.com/appframework/egore-personal/pagespeed/compare/1.0.9...1.0.10) (2024-12-02)


### Bug Fixes

* **deps:** update dependency org.jsoup:jsoup to v1.18.2 ([bea4c88](https://gitlab.com/appframework/egore-personal/pagespeed/commit/bea4c8888f1bd7429164c15b988403585506eacc))

## [1.0.9](https://gitlab.com/appframework/egore-personal/pagespeed/compare/1.0.8...1.0.9) (2024-11-28)

## [1.0.8](https://gitlab.com/appframework/egore-personal/pagespeed/compare/1.0.7...1.0.8) (2024-11-27)

## [1.0.7](https://gitlab.com/appframework/egore-personal/pagespeed/compare/1.0.6...1.0.7) (2024-11-20)

## [1.0.6](https://gitlab.com/appframework/egore-personal/pagespeed/compare/1.0.5...1.0.6) (2024-11-15)

## [1.0.5](https://gitlab.com/appframework/egore-personal/pagespeed/compare/1.0.4...1.0.5) (2024-11-07)

## [1.0.4](https://gitlab.com/appframework/egore-personal/pagespeed/compare/1.0.3...1.0.4) (2024-11-07)

## [1.0.3](https://gitlab.com/appframework/egore-personal/pagespeed/compare/1.0.2...1.0.3) (2024-11-06)

## [1.0.2](https://gitlab.com/appframework/egore-personal/pagespeed/compare/1.0.1...1.0.2) (2024-11-06)

## [1.0.1](https://gitlab.com/appframework/egore-personal/pagespeed/compare/1.0.0...1.0.1) (2024-10-23)

# 1.0.0 (2024-10-22)


### Bug Fixes

* **deps:** update dependency org.jsoup:jsoup to v1.18.1 ([a4d8e44](https://gitlab.com/appframework/egore-personal/pagespeed/commit/a4d8e441d9a509aa9489af1e2d9b98c5cf939abc))
* **deps:** update dependency web-vitals to v4 ([121ef3f](https://gitlab.com/appframework/egore-personal/pagespeed/commit/121ef3fb1415aef2d2524f951181d0f9d73a0946))
* **ui:** Don't use rsync ([5df8d64](https://gitlab.com/appframework/egore-personal/pagespeed/commit/5df8d647b98ac4792c5a198beb3bb6855a835989))
* **ui:** Fix errors from JSX babeling ([4f3ffa2](https://gitlab.com/appframework/egore-personal/pagespeed/commit/4f3ffa2535dfc7e309692ad88d3a5834c006e355))


### Features

* Replace react UI by Vue 3 ([9c930e1](https://gitlab.com/appframework/egore-personal/pagespeed/commit/9c930e1fcc55d52476ef0fd13f3fe85e103c8425))
* **test:** Add jacoco coverage ([5686f9a](https://gitlab.com/appframework/egore-personal/pagespeed/commit/5686f9ae882eb92192b5cb202274913f1147dae8))
