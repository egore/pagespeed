/* tslint:disable */
/* eslint-disable */

export interface HTMLMeasurement extends Measurement {
    styles: URLMeasurement[];
    scripts: URLMeasurement[];
    images: URLMeasurement[];
}

export interface Measurement extends PanacheEntity {
    url: string;
    responseCode: number;
    contentSize: number;
    duration: number;
    timestamp: Date;
    fullContentSize: number;
}

export interface URLMeasurement extends Measurement {
}

export interface PanacheEntity extends PanacheEntityBase {
    id: number;
}

export interface PanacheEntityBase {
}
