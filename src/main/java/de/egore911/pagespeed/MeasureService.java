package de.egore911.pagespeed;

import java.io.IOException;
import java.util.List;

import jakarta.transaction.Transactional;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import de.egore911.pagespeed.model.HTMLMeasurement;
import org.hibernate.Hibernate;

@Path("/measurements")
public class MeasureService {
	
    @GET
    @Path("check")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public Long check() throws IOException {
    	HTMLMeasurement m = new HTMLMeasurement();
    	m.url = "https://www.christophbrill.de/";
		m.measure();
		return m.duration;
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public List<HTMLMeasurement> list() {
        var measurements = HTMLMeasurement.<HTMLMeasurement>listAll();
        for (var measurement : measurements) {
            Hibernate.initialize(measurement.styles);
            Hibernate.initialize(measurement.scripts);
            Hibernate.initialize(measurement.images);
        }
        return measurements;
    }

}