package de.egore911.pagespeed.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinTable;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

@Entity
@Table(name = "html")
public class HTMLMeasurement extends Measurement {

	@OneToMany
	@JoinTable(name = "html_styles", joinColumns = {
			@JoinColumn(name = "html_id") }, inverseJoinColumns = {
			@JoinColumn(name = "style_id") })
	public List<URLMeasurement> styles = new ArrayList<>();
	@OneToMany
	@JoinTable(name = "html_scripts", joinColumns = {
			@JoinColumn(name = "html_id") }, inverseJoinColumns = {
			@JoinColumn(name = "script_id") })
	public List<URLMeasurement> scripts = new ArrayList<>();
	@OneToMany
	@JoinTable(name = "html_images", joinColumns = {
			@JoinColumn(name = "html_id") }, inverseJoinColumns = {
			@JoinColumn(name = "image_id") })
	public List<URLMeasurement> images = new ArrayList<>();

	@Override
	public long getFullContentSize() {
		return contentSize + styles.stream().mapToInt(x -> x.contentSize).sum()
				+ scripts.stream().mapToInt(x -> x.contentSize).sum()
				+ images.stream().mapToInt(x -> x.contentSize).sum();
	}

	public void measure() throws IOException {
		long start = System.currentTimeMillis();
		Connection connect = Jsoup.connect(url);
		Connection.Response response = connect.execute();
		duration = System.currentTimeMillis() - start;
		responseCode = response.statusCode();
		contentSize = response.bodyAsBytes().length;

		Document doc = response.parse();

		Elements links = doc.select("link");
		for (Element element : links) {
				styles.add(createURLMeasurement(element, "href"));
		}

		Elements scripts = doc.select("script");
		for (Element element : scripts) {
			this.scripts.add(createURLMeasurement(element, "href"));
		}

		Elements imgs = doc.select("img");
		for (Element element : imgs) {
			images.add(createURLMeasurement(element, "href"));
		}

		persist();

	}

	private URLMeasurement createURLMeasurement(Element element, String attribute) throws IOException {
		URLMeasurement style = new URLMeasurement();
		style.url = element.attr(attribute);
		if (!style.url.startsWith("http")) {
			style.url = url + style.url;
		}
		style.measure();
		return style;
	}
}
