package de.egore911.pagespeed.model;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "url")
public class URLMeasurement extends Measurement {

	@Override
	public long getFullContentSize() {
		return contentSize;
	}

	@Override
	public void measure() throws IOException {
		URL url = new URL(this.url);
		long start = System.currentTimeMillis();
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		int statusCode = connection.getResponseCode();
		duration = System.currentTimeMillis() - start;
		responseCode = statusCode;
		contentSize = getSize(connection.getInputStream());
		persist();
	}

	private static int getSize(InputStream inputStream) throws IOException {
		byte[] buffer = new byte[4096];
		int bytesRead = 0;
		int result = 0;
		while ((bytesRead = inputStream.read(buffer)) != -1) {
			result += bytesRead;
		}
		return result;
	}

}
