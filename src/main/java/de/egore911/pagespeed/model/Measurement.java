package de.egore911.pagespeed.model;

import java.io.IOException;
import java.time.LocalDateTime;

import jakarta.persistence.MappedSuperclass;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

@MappedSuperclass
public abstract class Measurement extends PanacheEntity {

	/**
	 * URL of the resource to load
	 */
    public String url;
    /**
     * HTTP response code of the loaded resource
     */
    public int responseCode;
    /**
     * Size in bytes of the loaded resource
     */
    public int contentSize;
    /**
     * Duration it took in to load the resource in milliseconds
     */
    public long duration;
    /**
     * Timestamp of the execution (defaults to {@code now()})
     */
    public LocalDateTime timestamp = LocalDateTime.now();

    public abstract long getFullContentSize();
    
    public abstract void measure() throws IOException;

}