package de.egore911.pagespeed;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

@QuarkusTest
public class MeasureServiceTest {

    @Test
    public void testHelloEndpoint() {
        given()
          .when().get("/measurements")
          .then()
             .statusCode(200)/*
             .body(is("Hello RESTEasy"))*/;
    }

}