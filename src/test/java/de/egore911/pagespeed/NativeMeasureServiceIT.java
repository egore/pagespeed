package de.egore911.pagespeed;

import io.quarkus.test.junit.QuarkusIntegrationTest;

@QuarkusIntegrationTest
public class NativeMeasureServiceIT extends MeasureServiceTest {

    // Execute the same tests but in native mode.
}