# Pagespeed - Measure response duration and size of a webpage

This is a fun project to experiment with React and Mutiny. It is meant for
educational purposes only and should not be used in production

## Scope

This project offers two endpoints, one to perform a measurement and one to
display all performed measurements.

To perform a measurement you can invoke the following endpoint. It will display
the time it took to load the webpage.

    http://localhost:8800/measurements/check

The other endpoint is invoked from the UI. It will return all known measurements

    http://localhost:8800/measurements

## Data storage

All measurements are stored using Hibernate into a (temporary) PostgreSQL
database. This is to evaluate the possibilities of [testcontainers](https://www.testcontainers.org/)
and obviously not a long term storage.

## Periodic data retrieval

As of now using quartz or similar to perform periodic querying of the webpage is
not part of this experiment. To generate some test data you can use

    watch -n 60 curl http://localhost:8800/measurements/check
